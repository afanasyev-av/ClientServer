#-------------------------------------------------
#
# Project created by QtCreator 2016-07-07T11:16:19
#
#-------------------------------------------------

QT       += core gui
QT += network
QT += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SignalServer
TEMPLATE = app


SOURCES += main.cpp\
        servermainwindow.cpp

HEADERS  += servermainwindow.h

FORMS    += servermainwindow.ui

CONFIG += mobility
MOBILITY = 

