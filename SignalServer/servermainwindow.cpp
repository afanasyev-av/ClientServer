#include "servermainwindow.h"
#include "ui_servermainwindow.h"

ServerMainWindow::ServerMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ServerMainWindow)
{
    //сервер
    int nPort;
    nPort = 9090;
    ui->setupUi(this);
    m_ptcpServer = new QTcpServer(this);
    if (!m_ptcpServer->listen(QHostAddress::Any, nPort)) {
        QMessageBox::critical(0, "Server Error",
                              "Unable to start the server:"  + m_ptcpServer->errorString());
        m_ptcpServer->close();
        return;
    }
    connect(m_ptcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));
    ui->plainTextEdit->appendPlainText("server start\n");
    //IP адрес
    QList<QHostAddress> addr = QNetworkInterface::allAddresses();
    int i;
    for(i=0;i<addr.size();i++){
        ui->comboBox->addItem(addr[i].toString());
    }
    //БД
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QCoreApplication::applicationDirPath() +"/clients.sqlite");

    if(!db.open()){
        QString name = db.lastError().text();
        QMessageBox::information(0,"info",name);
    }

    model = new QSqlTableModel;
    //привязываем к таблице
    model->setTable("clients");
    //стратегия редактирования таблицы
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    //заносим данные
    model->select();
    model->setHeaderData(0, Qt::Horizontal, "ID");
    model->setHeaderData(1, Qt::Horizontal, "name");
    model->setHeaderData(2, Qt::Horizontal, "IP");
    model->setHeaderData(3, Qt::Horizontal, "port");

    ui->tableView->setModel(model);
    ui->tableView->show();
}

ServerMainWindow::~ServerMainWindow()
{
    delete ui;
}

/*virtual*/ void ServerMainWindow::slotNewConnection()
{
    QTcpSocket* pClientSocket = m_ptcpServer->nextPendingConnection();
    connect(pClientSocket, SIGNAL(disconnected()), pClientSocket, SLOT(deleteLater()));
    connect(pClientSocket, SIGNAL(readyRead()), this, SLOT(slotReadClient()));
    ui->plainTextEdit->appendPlainText("client connection\n");
    //sendToClient(pClientSocket, "Server Response: Connected!");
}

void ServerMainWindow::slotReadClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    QString str;
    str = pClientSocket->readAll();

    //QMessageBox::information(0,"client",str);
    ui->plainTextEdit->appendPlainText("client recv: "+str+"\n");

    QStringList stritem = str.split(" ");

    int row;
    QSqlTableModel modelfind;
    modelfind.setTable("clients");
    //стратегия редактирования таблицы
    modelfind.setEditStrategy(QSqlTableModel::OnFieldChange);
    modelfind.setFilter(QString("name LIKE '%1'").arg( stritem[0] ));
    modelfind.select();

    if(modelfind.rowCount()>=1){
        modelfind.setData(modelfind.index(0, 1), stritem[0]);
        modelfind.setData(modelfind.index(0, 2), stritem[1]);
        modelfind.setData(modelfind.index(0, 3), stritem[2]);
        modelfind.submitAll();
        modelfind.database().commit();
        modelfind.select();
        model->select();
    }
    else{
        row = model->rowCount();
        model->insertRow(row);
        model->setData(model->index(row, 0), QString("%1").arg(row+1));//Пустая строка
        model->setData(model->index(row, 1), stritem[0]);
        model->setData(model->index(row, 2), stritem[1]);
        model->setData(model->index(row, 3), stritem[2]);
        model->submitAll();
        model->database().commit();
        model->select();
    }
    str = "";
    for(row=0;row<model->rowCount();row++){
        str += model->record(row).value("name").toString()+" "+
               model->record(row).value("IP").toString()+" "+
               model->record(row).value("port").toString();
        if(row != model->rowCount()-1){
            str+= " ";
        }
    }
    sendToClient(pClientSocket,str);
}

void ServerMainWindow::sendToClient(QTcpSocket* pSocket, const QString& str)
{
    QByteArray  arrBlock;
    //QDataStream out(&arrBlock, QIODevice::WriteOnly);
    //out.setVersion(QDataStream::Qt_5_2);
    //out << str;
    arrBlock = str.toUtf8();
    pSocket->write(arrBlock);
}
