#ifndef SERVERMAINWINDOW_H
#define SERVERMAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QNetworkInterface>
#include <QList>
#include <QStringList>

namespace Ui {
class ServerMainWindow;
}

class ServerMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QTcpServer* m_ptcpServer;
    QSqlTableModel *model;

    explicit ServerMainWindow(QWidget *parent = 0);
    ~ServerMainWindow();
    void sendToClient(QTcpSocket* pSocket, const QString& str);

public slots:
    virtual void slotNewConnection();
            void slotReadClient   ();

private:
    Ui::ServerMainWindow *ui;
};

#endif // SERVERMAINWINDOW_H
