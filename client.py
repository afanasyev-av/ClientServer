#!/usr/bin/env python
# -*- coding: cp1251 -*-


from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
import stun
from socket import *
import sys

def SendSignalServer(name_client,IP_sigserver):
    #�������� ���� ���� � ip
    nat_type, external_ip, external_port = stun.get_ip_info()
    print "real IP: ", external_ip, external_port
    #����������� ���������� ����� IP � PORT ����������� ������� ��� �������� �������
    host_sigserver = IP_sigserver
    port_sigserver = 9090
    addr_sigserv = (host_sigserver,port_sigserver)
    data_out = name_client + ' ' + external_ip + ' ' + str(external_port)
    try:
        tcp_socket = socket(AF_INET, SOCK_STREAM)
        local_ip = gethostbyname_ex(gethostname())[2]
        print "local IP: ", local_ip
        tcp_socket.connect(addr_sigserv)
        tcp_socket.send(data_out)

        data_in = tcp_socket.recv(1024)
        liststr = data_in.split(' ');
        client_dict = {}
        for i in range(len(liststr)/3):
            client_dict[liststr[3*i]] = (liststr[3*i+1],int(liststr[3*i+2]))
        print "clients list: ",client_dict
        return client_dict
    except:
        print "Exit" 
        sys.exit(1)
    
    finally:
        if(tcp_socket != ''):
            tcp_socket.close()

class EchoUDPClient(DatagramProtocol):
    IP = "XXX.XXX.XXX.XXX"
    ort = 12345
    strings = [
        "hello1",
        "hello2",
        "hello3"
    ]
    def __init__(self,ip,port):
        self.IP = ip
        self.port = port
    
    def startProtocol(self):
        print "start"
        self.transport.connect(self.IP, self.port)
        self.sendDatagram()
    
    def sendDatagram(self):
        print "send"
        if len(self.strings):
            datagram = self.strings.pop(0)
            self.transport.write(datagram)
        else:
            reactor.stop()

    def datagramReceived(self, datagram, host):
        print "recv"
        print 'Datagram received: ', repr(datagram)
        self.sendDatagram()

def main_client():
    client_name = raw_input("login: ")
    IP_sigserver = raw_input("IP signal server: ")
    clientlist = SendSignalServer(client_name,IP_sigserver)
    second_client = raw_input("select the person: ")
    ip,port = clientlist[second_client]
    print ip, port
    protocol = EchoUDPClient(ip,port)
    t = reactor.listenUDP(0, protocol)
    reactor.run()
    
class EchoUDPServer(DatagramProtocol):
    def datagramReceived(self, datagram, address):
        self.transport.write(datagram, address)

def main_server():
    client_name = raw_input("login: ")
    IP_sigserver = raw_input("IP signal server: ")
    clientlist = SendSignalServer(client_name,IP_sigserver)
    reactor.listenUDP(8000, EchoUDPServer())
    reactor.run()
        
if __name__ == '__main__':
    app = raw_input("server or client: ")
    if(app == 'c'):
        main_client()
    elif (app == 's'):
        main_server()
    else:
        print "exit"
